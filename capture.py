# import sys
# import os
# import matplotlib
import numpy as np
# matplotlib.use('TkAgg')
# import matplotlib.pyplot as plt
# import copy
import cv2

cap = cv2.VideoCapture(0)

gesture = 'dataset/Space/Space'
i = 0
count = 0
capture = False

while True:
    ret, frame = cap.read()

    roi=frame[100:300, 150:350]

    cv2.rectangle(frame,(150,100),(350,300),(0,255,0),0)

    cv2.imshow('frame', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

    if cv2.waitKey(33) == ord('a'):
        capture = True
        print('starting capturing....')

    if capture:
        i = i + 1
        if i == 4:
            count = count + 1
            print("captured {} images.".format(count))
            cv2.imwrite(gesture + str(count) + '.jpg',roi)
            i = 0

# Following line should... <-- This should work fine now
cv2.destroyAllWindows() 
cap.release()
